import 'package:evertv/models/article.dart';
import 'package:evertv/pages/article_details.dart';
import 'package:flutter/material.dart';

class ArticleCard extends StatelessWidget {
  final Article article;
  const ArticleCard({Key key,this.article}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (_)=>ArticleDetails(article: article,)));
      },
      child: Card(
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Image.asset(
                article.imageUrl,
                height: 80,
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(article.title,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  Text(article.subtitle,
                  style: const TextStyle(
                    fontSize: 12
                  ),)
                ],
              ),
            ),
            Text(article.date,style: const TextStyle(
              fontSize: 10
            ),)
          ],
        ),
      ),
    );
  }
}
