import 'dart:io';
import 'dart:typed_data';

import 'package:evertv/constants/strings.dart';
import 'package:evertv/pages/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


import 'app_route_observer.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({@required this.permanentlyDisplay, Key key})
      : super(key: key);

  final bool permanentlyDisplay;

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> with RouteAware {
  String _selectedRoute;
  AppRouteObserver _routeObserver;

  Alignment position = Alignment.centerLeft;

  @override
  void initState() {
    super.initState();
    _routeObserver = AppRouteObserver();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    _routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    _updateSelectedRoute();
  }

  @override
  void didPop() {
    _updateSelectedRoute();
  }

  @override
  Widget build(BuildContext context) {
    final strings = AppStrings();

    return Builder(
      builder: (ctx) {
        Widget widget;

        widget = SafeArea(
          child: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                DrawerHeader(
                  curve: Curves.bounceInOut,
                  duration: Duration(seconds: 5),
                  child: Stack(
                    children: [
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.end,
                      //   children: [
                      //     IconButton(
                      //       icon: Icon(
                      //         Icons.clear,
                      //         color: AppColors().successGreen,
                      //       ),
                      //       onPressed: () {
                      //         Navigator.pop(context);
                      //       },
                      //     ),
                      //   ],
                      // ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ],
                  ),
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(58, 170, 53, 0.1),
                  ),
                ),
                drawerNavOption(
                    "Vérifications", "verification",HomePage.route),

               /* Row(
                  children: [
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      onTap: () async {
                        final url = "https://www.facebook.com/RMobility";
                        _launchURL(url);
                      },
                      child: SvgPicture.asset(
                        "assets/svg/drawer_facebook.svg",
                        height: 36,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      onTap: () {
                        String url =
                            "https://www.instagram.com/rmobility001/?fbclid=IwAR120r7bn8o-p2AKE_dR1r5deUvZUNLZkXTY-tjlosX7EFiZldQc5Zse6g4";
                        _launchURL(url);
                      },
                      child: SvgPicture.asset(
                        "assets/svg/instagram.svg",
                        height: 32,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      onTap: () {
                        String url = "https://wa.me/message/VESAXJQ6L34DJ1";
                        _launchURL(url);
                      },
                      child: SvgPicture.asset(
                        "assets/svg/drawer_whatsapp.svg",
                        height: 36,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      onTap: () {
                        final Uri _emailLaunchUri = Uri(
                          scheme: 'mailto',
                          path: 'rmobility@raynis.co',
                        );

                        _launchURL(_emailLaunchUri.toString());
                      },
                      child: SvgPicture.asset(
                        "assets/svg/drawer_message.svg",
                        height: 36,
                      ),
                    ),
                  ],
                ),*/
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  child: Text(
                    "Joindre la Team RMobility",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
        return widget;
      },
    );
  }

  Future<void> _navigateTo(BuildContext context, String routeName) async {
    Navigator.pop(context);
    await Navigator.pushNamed(context, routeName);
  }

  void _updateSelectedRoute() {
    setState(() {
      _selectedRoute = ModalRoute.of(context).settings.name;
    });
  }


  Widget drawerNavOption(String title, String icon, String route) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Colors.white.withOpacity(0.1),
        highlightColor: Colors.transparent,
        onTap: () {
          _navigateTo(context, route);
        },
        child: Container(
          margin: const EdgeInsets.only(right: 15, top: 5, bottom: 5, left: 20),
          height: 44,
          child: Row(
            children: [
              Text(
                title,
                style: TextStyle(
                  color: Colors.black,
                  //fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
