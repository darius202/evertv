import 'package:flutter/material.dart';

class AppColors{
  final Color primaryBlue = const Color(0xFF0D49A8);
  final Color second = const Color(0xFF5B5C5D);
  final Color unselected = const Color(0xFFE6E6E6);
  final Color gray = const Color(0xFF999999);
  final Color grayLight = const Color(0xFF666666);
  final Color grayLight2 = const Color(0xFFCCCCCC);
  final Color grayLight3 = const Color(0xFFE7E7ED);
  final Color grayLight6 = const Color(0xFFF2F2F2);
  final Color dark1 = const Color(0xFF30313F);
  final Color dark2 = const Color(0xFF393A4A);
}