class AppStrings {
  final String appName = "Ever TV";
  final String onBoardingOneTitle = "Ever tv informations";
  final String onBoardingOneSubtitle = "S'informer en temps réels";
  final String onBoardingTwoTitle = "Infos du monde ";
  final String onBoardingTwoSubtitle = "Rester informer à travers le monde";
  final String onBoardingThreeTitle = "Lecture";
  final String onBoardingThreeSubtitle = "Presse écrite ";
  //static const String baseUrl = "http://localhost:1337";

  AppStrings();
}
