import 'package:evertv/constants/strings.dart';
import 'package:evertv/pages/politique.dart';
import 'package:evertv/pages/football.dart';
import 'package:evertv/pages/health.dart';
import 'package:evertv/pages/home.dart';
import 'package:evertv/pages/onboarding.dart';
import 'package:evertv/pages/search.dart';
import 'package:flutter/material.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
   MyApp({Key key}) : super(key: key);
  final strings = AppStrings();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: strings.appName,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const OnBoardingPage(),
      routes: {
        HomePage.route:(_)=> const HomePage(),
        OnBoardingPage.route:(_)=> const OnBoardingPage(),
        SearchPage.route:(_)=> SearchPage(),
        HealthPage.route:(_)=> const HealthPage(),
        PolitiquePage.route:(_)=> const PolitiquePage(),
        FootballPage.route:(_)=> const FootballPage(),
      },
    );
  }
}