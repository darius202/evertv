import 'package:evertv/components/article_card.dart';
import 'package:evertv/constants/colors.dart';
import 'package:evertv/constants/styles.dart';
import 'package:evertv/models/article.dart';
import 'package:evertv/pages/search.dart';
import 'package:flutter/material.dart';

class HealthPage extends StatefulWidget {
  static const route = "/health";
  const HealthPage({Key key}) : super(key: key);
  @override
  _HealthPageState createState() => _HealthPageState();
}

class _HealthPageState extends State<HealthPage> {
  final colors = AppColors();
  List<Article> articles = [];
  Article initArticle = Article();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchArticle();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: colors.primaryBlue,
        title: const Text("Santé",
          style: AppStyles.appBarTitle,
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: const Icon(Icons.search,color: Colors.black,),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (_)=>SearchPage(articles: articles)));
            },
          )
        ],
      ),
      body: ListView.builder(
          itemCount: articles.length,
          itemBuilder: (context,index){
            return ArticleCard(article: articles[index],);
          }
      ),
    );
  }

  void _fetchArticle(){
    initArticle.id = 0;
    initArticle.title = "Dernières nouvelles";
    initArticle.subtitle = "Toutes les personnes vaccinées mourront dans 2 ans";
    initArticle.date = "ven.04.dec.2021";
    initArticle.imageUrl = "assets/images/doctor.png";
    setState(() {
      articles.add(initArticle);
    });
    initArticle.id = 0;
    initArticle.title = "Dernières nouvelles";
    initArticle.subtitle = "Toutes les personnes vaccinées mourront dans 2 ans";
    initArticle.date = "ven.04.dec.2021";
    initArticle.imageUrl = "assets/images/doctor.png";
    setState(() {
      articles.add(initArticle);
    });
    initArticle.id = 0;
    initArticle.title = "Dernières nouvelles";
    initArticle.subtitle = "Toutes les personnes vaccinées mourront dans 2 ans";
    initArticle.date = "ven.04.dec.2021";
    initArticle.imageUrl = "assets/images/doctor.png";
    setState(() {
      articles.add(initArticle);
    });
    initArticle.id = 0;
    initArticle.title = "Dernières nouvelles";
    initArticle.subtitle = "Toutes les personnes vaccinées mourront dans 2 ans";
    initArticle.date = "ven.04.dec.2021";
    initArticle.imageUrl = "assets/images/doctor.png";
    setState(() {
      articles.add(initArticle);
    });
  }

}
