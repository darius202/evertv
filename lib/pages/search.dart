import 'package:evertv/components/article_card.dart';
import 'package:evertv/constants/colors.dart';
import 'package:evertv/constants/styles.dart';
import 'package:evertv/models/article.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  static const route = "/search";
  List<Article> articles;
  SearchPage({Key key,this.articles}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<Article> filtreArticles = [];
  @override
  Widget build(BuildContext context) {
    final colors = AppColors();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: colors.primaryBlue,
        title: Center(
          child: TextFormField(
            onChanged: (value){
              setState(() {
                filtreArticles = widget.articles.where((element) => (element.title.toLowerCase().contains(value) || element.subtitle.contains(value))).toList();
              });
            },
            autofocus: true,
            decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: "Rechercher",
              hintStyle: AppStyles.hinStyle
            ),
          ),
        ),
      ),
      body:filtreArticles.isEmpty? const Center(
        child: Text("Aucun résultat à afficher",style: TextStyle(
          color:Colors.black,
        ),),
      ):ListView.builder(
          itemCount: filtreArticles.length,
          itemBuilder: (context,index){
            return ArticleCard(article: filtreArticles[index]);
          }
      ),
    );
  }
}
