import 'package:evertv/components/article_card.dart';
import 'package:evertv/constants/colors.dart';
import 'package:evertv/constants/styles.dart';
import 'package:evertv/models/article.dart';
import 'package:evertv/pages/search.dart';
import 'package:flutter/material.dart';

class PolitiquePage extends StatefulWidget {
  static const route = "/affaire";
  const PolitiquePage({Key key}) : super(key: key);
  @override
  _PolitiquePageState createState() => _PolitiquePageState();
}

class _PolitiquePageState extends State<PolitiquePage> {
  final colors = AppColors();
  List<Article> articles = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchArticle();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: colors.primaryBlue,
        title: const Text("Politique",
          style: AppStyles.appBarTitle,
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: const Icon(Icons.search,color: Colors.black,),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (_)=>SearchPage(articles: articles)));
            },
          )
        ],
      ),
      body: ListView.builder(
        itemCount: articles.length,
          itemBuilder: (context,index){
            return ArticleCard(article: articles[index],);
          }
      ),
    );
  }

  void _fetchArticle(){
    Article initArticle1 = Article();
    initArticle1.id = 0;
    initArticle1.title = "Opposants politique en prison au Bénin";
    initArticle1.subtitle = "La demande d'un député français à Emmanuel Macron";
    initArticle1.date = "jeu.03.dec.2021";
    initArticle1.imageUrl = "assets/images/papers.png";
      articles.add(initArticle1);

    Article initArticle2 = Article();
    initArticle2.id = 1;
    initArticle2.title = "Cotonou";
    initArticle2.subtitle = "Le préfet abroge son arrêté pris contre les tokpa-tokpa";
    initArticle2.date = "jeu.03.dec.2021";
    initArticle2.imageUrl = "assets/images/papers.png";
      articles.add(initArticle2);

    Article initArticle3 = Article();
    initArticle3.id = 2;
    initArticle3.title = "Saîf Al-Islam kadhafi";
    initArticle3.subtitle = "La justice autorise le fils de Kadhafi à pariciper à la présidentielle en Libie";
    initArticle3.date = "jeu.03.dec.2021";
    initArticle3.imageUrl = "assets/images/papers.png";
      articles.add(initArticle3);

    Article initArticle4 = Article();
    initArticle4.id = 3;
    initArticle4.title = "France";
    initArticle4.subtitle = "Notre pays peut-il pas agir en faveur des exilés politique au Bénin ?";
    initArticle4.date = "jeu.03.dec.2021";
    initArticle4.imageUrl = "assets/images/papers.png";
      articles.add(initArticle4);

    Article initArticle5 = Article();
    initArticle5.id = 4;
    initArticle5.title = "Le vote à bille de cristal";
    initArticle5.subtitle = "Le moyen de fiabilité des élections en Gambie";
    initArticle5.date = "jeu.03.dec.2021";
    initArticle5.imageUrl = "assets/images/papers.png";
      articles.add(initArticle5);

    Article initArticle6 = Article();
    initArticle6.id = 5;
    initArticle6.title = "Attaque Djihadiste au Bénin";
    initArticle6.subtitle = "Le président du parti NFN invite les autorités à la vigilance et à la prudence";
    initArticle6.date = "ven.04.dec.2021";
    initArticle6.imageUrl = "assets/images/papers.png";
      articles.add(initArticle6);

  }
}
