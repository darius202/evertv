import 'dart:convert';
import 'dart:typed_data';

import 'package:evertv/constants/colors.dart';
import 'package:evertv/constants/strings.dart';
import 'package:evertv/pages/home.dart';
import 'package:flutter/material.dart';


class OnBoardingPage extends StatefulWidget {
  static const route = "/onboarding";

  const OnBoardingPage({Key key}) : super(key: key);
  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  final _controller = PageController(initialPage: 0);
  int currentPage = 0;

  final colors = AppColors();
  final strings = AppStrings();


  @override
  void initState() {
    //initFB();
  }

  void initFB() async {
    //await Firebase.initializeApp();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).padding.top + 40,
          ),
          Expanded(
            child: PageView(
              controller: _controller,
              onPageChanged: (value) {
                setState(() {
                  currentPage = value;
                });
              },
              children: <Widget>[pageOne(), pageTwo(), pageThree()],
            ),
          ),
          _buildIndicators(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Navigator.pushReplacementNamed(context, HomePage.route);
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
                  child: Row(
                    children: [
                      Text(
                        "Passer",
                        style: TextStyle(
                          color: colors.primaryBlue,
                        ),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: colors.primaryBlue, shape: BoxShape.circle),
                        child: const Icon(
                          Icons.arrow_forward,
                          size: 12,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget pageOne() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 40,
            ),
            Center(child: Image.asset("assets/images/news.png",height: 200,)),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 25,
                horizontal: 40,
              ),
              child: Text(
                strings.onBoardingOneTitle,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 40),
              child: Text(
                strings.onBoardingOneSubtitle,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 15,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget pageTwo() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 40,
            ),
            Image.asset("assets/images/world.png",height: 200,),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 25,
                horizontal: 40,
              ),
              child: Text(
                strings.onBoardingTwoTitle,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 40),
              child: Text(
                strings.onBoardingTwoSubtitle,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 15,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget pageThree() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 40,
            ),
            Image.asset("assets/images/papers.png",height: 200,),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 25,
                horizontal: 40,
              ),
              child: Text(
                strings.onBoardingThreeTitle,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 40),
              child: Text(
                strings.onBoardingThreeSubtitle,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 15,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }


  Widget _buildIndicators() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(3, (index) {
          return Container(
            margin: const EdgeInsets.symmetric(horizontal: 4),
            height: 10,
            width: 10,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: currentPage == index
                  ? colors.primaryBlue
                  : colors.primaryBlue.withOpacity(0.4),
            ),
          );
        }),
      ),
    );
  }
}
