import 'package:evertv/constants/colors.dart';
import 'package:evertv/constants/strings.dart';
import 'package:evertv/constants/styles.dart';
import 'package:evertv/models/article.dart';
import 'package:evertv/pages/politique.dart';
import 'package:evertv/pages/football.dart';
import 'package:evertv/pages/search.dart';
import 'package:flutter/material.dart';

import 'health.dart';

class HomePage extends StatefulWidget {
  static const route = "/home";
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final strings = AppStrings();
  final colors = AppColors();

  @override
  Widget build(BuildContext context) {
    const double runSpacing = 15;
    const double spacing = 15;
    const columns = 2;
    final w = (MediaQuery.of(context).size.width - runSpacing * (columns - 1)) /
        columns -
        20;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(strings.appName,
          style: AppStyles.appBarTitle
        ),
        backgroundColor: colors.primaryBlue,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: SingleChildScrollView(
                child: Wrap(
                  runSpacing: runSpacing,
                  spacing: spacing,
                  alignment: WrapAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, PolitiquePage.route);
                      },
                      child: Container(
                        width: w,
                        height: w,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          children: <Widget>[
                            const SizedBox(
                              height: 25,
                            ),
                            Expanded(
                              child: Image.asset(
                                "assets/images/politique.jpg",
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              "Politique",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, HealthPage.route);
                      },
                      child: Container(
                        width: w,
                        height: w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          children: <Widget>[
                            const SizedBox(
                              height: 25,
                            ),
                            Expanded(
                              child: Image.asset(
                                "assets/images/health.jpg",
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              "Santé",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, FootballPage.route);
                      },
                      child: Container(
                        width: w,
                        height: w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          children: <Widget>[
                            const SizedBox(
                              height: 15,
                            ),
                            Expanded(
                              child: Image.asset(
                                "assets/images/goal.png",
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              "Football",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, FootballPage.route);
                      },
                      child: Container(
                        width: w,
                        height: w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          children: <Widget>[
                            const SizedBox(
                              height: 15,
                            ),
                            Expanded(
                              child: Image.asset(
                                "assets/images/affaires.png",
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              "Affaires",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, FootballPage.route);
                      },
                      child: Container(
                        width: w,
                        height: w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          children: <Widget>[
                            const SizedBox(
                              height: 15,
                            ),
                            Expanded(
                              child: Image.asset(
                                "assets/images/entreprise.png",
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              "Business",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, FootballPage.route);
                      },
                      child: Container(
                        width: w,
                        height: w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          children: <Widget>[
                            const SizedBox(
                              height: 15,
                            ),
                            Expanded(
                              child: Image.asset(
                                "assets/images/diversite.png",
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              "Divers",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

}
