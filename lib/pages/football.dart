import 'package:evertv/components/article_card.dart';
import 'package:evertv/constants/colors.dart';
import 'package:evertv/constants/styles.dart';
import 'package:evertv/models/article.dart';
import 'package:evertv/pages/search.dart';
import 'package:flutter/material.dart';

class FootballPage extends StatefulWidget {
  static const route = "/business";
  const FootballPage({Key key}) : super(key: key);

  @override
  _FootballPageState createState() => _FootballPageState();
}

class _FootballPageState extends State<FootballPage> {
  final colors = AppColors();
  List<Article> articles = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchArticle();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: colors.primaryBlue,
        title: const Text("Football",
          style: AppStyles.appBarTitle,
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: const Icon(Icons.search,color: Colors.black,),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (_)=>SearchPage(articles: articles)));
            },
          )
        ],
      ),
      body: ListView.builder(
          itemCount: articles.length,
          itemBuilder: (context,index){
            return ArticleCard(article: articles[index]);
          }
      ),
    );
  }

  void _fetchArticle(){
    Article initArticle1 = Article();
    initArticle1.id = 0;
    initArticle1.title = "Steve Mounié";
    initArticle1.subtitle = "Mon jeu de tête surpasse celui de tout le monde";
    initArticle1.date = "ven.04.dec.2021";
    initArticle1.imageUrl = "assets/images/medal.png";
      articles.add(initArticle1);
      initArticle1.description = "Description de l'article ici";

    Article initArticle2 = Article();
    initArticle2.id = 1;
    initArticle2.title = "Championnat de volleyball au Bénin";
    initArticle2.subtitle = "La ligue Atlantique/Littoral célère ses équipes.";
    initArticle2.date = "ven.04.dec.2021";
    initArticle2.imageUrl = "assets/images/medal.png";
      articles.add(initArticle2);

    Article initArticle3 = Article();
    initArticle3.id = 2;
    initArticle3.title = "Coupe du monde Qatar 2022";
    initArticle3.subtitle = "La RD congo poursuit les éliminatoires";
    initArticle3.date = "ven.04.dec.2021";
    initArticle3.imageUrl = "assets/images/medal.png";
      articles.add(initArticle3);

    Article initArticle4 = Article();
    initArticle4.id = 3;
    initArticle4.title = "Fecafoot";
    initArticle4.subtitle = "Encore un soutien pour Samuel Eto'o";
    initArticle4.date = "ven.04.dec.2021";
    initArticle4.imageUrl = "assets/images/medal.png";
      articles.add(initArticle4);

    Article initArticle5 = Article();
    initArticle5.id = 4;
    initArticle5.title = "Ballon d'or 2021";
    initArticle5.subtitle = "La réponse de France Football à Messi qui plaidait la cause de Lewandoski";
    initArticle5.date = "ven.04.dec.2021";
    initArticle5.imageUrl = "assets/images/medal.png";
      articles.add(initArticle5);

    Article initArticle6 = Article();
    initArticle6.id = 5;
    initArticle6.title = "Clermont Foot";
    initArticle6.subtitle = "L'Ecureuil Jodel Dossou passeur décisif";
    initArticle6.date = "ven.04.dec.2021";
    initArticle6.imageUrl = "assets/images/medal.png";
      articles.add(initArticle6);

    Article initArticle7 = Article();
    initArticle7.id = 6;
    initArticle7.title = "PSG";
    initArticle7.subtitle = "Lionel Messi finalement apte ?";
    initArticle7.date = "ven.04.dec.2021";
    initArticle7.imageUrl = "assets/images/medal.png";
      articles.add(initArticle7);

    Article initArticle8 = Article();
    initArticle8.id = 7;
    initArticle8.title = "Bagarre entre Lukaku et Ibrahimmovic";
    initArticle8.subtitle = "une revanche en vue";
    initArticle8.date = "ven.04.dec.2021";
    initArticle8.imageUrl = "assets/images/medal.png";
      articles.add(initArticle8);

  }

}
