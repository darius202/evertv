import 'package:evertv/constants/colors.dart';
import 'package:evertv/constants/styles.dart';
import 'package:evertv/models/article.dart';
import 'package:flutter/material.dart';

class ArticleDetails extends StatefulWidget {
  final Article article;
  const ArticleDetails({Key key,this.article}) : super(key: key);

  @override
  _ArticleDetailsState createState() => _ArticleDetailsState();
}

class _ArticleDetailsState extends State<ArticleDetails> {
  final colors = AppColors();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: colors.primaryBlue,
        title:  Text(widget.article.title,
          style: AppStyles.appBarTitle,
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),
              Center(
                child: Image.asset(
                  widget.article.imageUrl,
                  height: 200,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
               Text(
                widget.article.title,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal:25.0),
                child: Text(
                    "Description de l'article ici avec tous les détails qu'il faut",
                  textAlign: TextAlign.center,
                ),
              )
            ],
          )
      ),
    );
  }
}
