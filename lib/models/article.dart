class Article {
  int id;
  String title;
  String subtitle;
  String imageUrl;
  String description;
  String date;

  Article(
      { this.id,
        this.subtitle,
        this.title,
        this.description,
        this.date,
        this.imageUrl
      });

  Article.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    subtitle = json['subtitle'];
    title = json['title'];
    description = json['description'];
    date = json['date'];
    imageUrl = json['imageUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['subtitle'] = this.subtitle;
    data['title'] = this.title;
    data['description'] = this.description;
    data['date'] = this.date;
    data['imageUrl'] = this.imageUrl;
    return data;
  }
}
